<?php

namespace Examples;

/**
 * Example class showing how to do bypass the business logic module entirely.
 */
class EmptyBusinessLogic implements \CreditCommons\BlogicInterface
{
    public function addRows(string $type, string $payee, string $payer, $quant, \stdClass $metadata, string $description = ''): array
    {
        return [];
    }
}
