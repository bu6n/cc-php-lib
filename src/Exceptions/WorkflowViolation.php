<?php

namespace CreditCommons\Exceptions;

/**
 * The workflow denies permission to the user to perform the action on the transaction.
 */
class WorkflowViolation extends CCViolation {

  public function __construct(
    // The Workflow ID
    public string $type,
    // The current state of the transaction
    public string $from,
    // The state to which the transaction may not be moved
    public string $to
  ) {
    parent::__construct();

  }

  function makeMessage() : string {
    if ($this->from == 'init') {
      return "$this->user was not allowed to create this '$this->type' transaction.";
    }
    else {
      return "On $this->node, $this->user was not allowed to move the '$this->type' transaction from state '$this->from' to '$this->to'.";
    }
  }
}
