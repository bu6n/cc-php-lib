<?php

namespace CreditCommons\Exceptions;

/**
 * Pass $acc_id, diff
 */
class MinLimitViolation extends TransactionLimitViolation {

  /**
   * {@inheritDoc}
   */
  function makeMessage() : string {
    $acc = $this->acc == '*' ? 'Balance of Trade' : $this->acc;
    return "This transaction would put account '$acc' on $this->node below its minium balance by $this->diff";
  }
}

