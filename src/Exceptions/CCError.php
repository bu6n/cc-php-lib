<?php

namespace CreditCommons\Exceptions;

use CreditCommons\Exceptions\CCFailure;

/**
 *
 * An throwable exception with public properties which can be sent via json and
 * reconstructed upstream.
 */
abstract class CCError Extends \Error implements \JsonSerializable {

  // These properties may be populated in __construct() and overridden in convertException().
  public string $node;
  public string $class;
  public string $method;
  public string $path;
  public string $user;
  public array $backtrace;

  /**
   * Generate an error on the source node, using its' $error_context;
   * @global type $error_context
   * @param type $message
   * @param type $code
   */
  function __construct($message = '', $code = 0) {
    global $error_context;
    parent::__construct($message, $code);
    $class_parts = explode('\\', get_called_class());
    $this->class = array_pop($class_parts);
    // Not needed by the client, which only reconstructs errors.
    // See NodeRequester::reconstructCCErr()
    foreach (['user', 'node', 'path', 'method'] as $prop) {
      $this->{$prop} = $error_context->{$prop};
    }
    $this->backtrace = $this->getTrace();
    $this->message = $this->makeMessage();
  }

  /**
   * This is instead of the final method getMessage() which is required for any \throwable
   * @return string
   */
  abstract function makeMessage() : string;

  /**
   * Before reconstructing the error make sure all the fields are present and
   * cast to the right type
   *
   * @note this is v similar to CreateFromValidatedStdClassTraitvalidateFields
   * except it takes an array and assumes no fields are missing.
   * @note this is called in CreditCommons\nodeRequester\reconstructCCException
   */
  static function validateCastParams(array &$fields) : void {
    $errs = [];
    foreach (self::getReflection()->getConstructor()->getParameters() as $param) {
      /** @var ReflectionParameter $param */
      $name = $param->getName();
      $type = $param->getType();
      $val = $fields[$name];
      // cast all the values
      switch($type) {
        case 'string':
          if ((string)$val == $val) {
            $fields[$name] = (string)$val;
            continue 2;
          }
          break;
        case 'int':
          if (is_numeric($val) and (int)$val == $val) {
            $fields[$name] = (int)$val;
            continue 2;
          }
          break;
        case 'float':
          if (is_numeric($val)) {
            $fields[$name] = (float)$val;
            continue 2;
          }
          break;
        case 'bool':
          if ((bool)($val) == $val) {
            $fields[$name] = (bool)$val;
            continue 2;
          }
          break;
        case 'array':
          if (is_iterable($val) or $val instanceOf \stdClass) {
            if (!is_array($val)) {
              $fields[$name] = (array)$val;
            }
            continue 2;
          }
        case 'CreditCommons\\Account':
          if ($val instanceOf \CreditCommons\Account) {
            continue 2;
          }
        default:
          throw new CCFailure("CCError does not recognise field type: $type");
      }
    }
  }

  /**
   * {@inheritDoc}
   */
  function jsonSerialize() :array {
    $array = [];
    foreach (self::getReflection()->getProperties() as $param) {
      if (substr($param->class, 0, 24) == 'CreditCommons\Exceptions') {
        $name = $param->getName();
        // Do we need to handle better what happens if $this->node is not set?
        if (!isset($this->{$name}))continue;
        $array[$name] = $this->{$name};
      }
    }
    unset($array['trace']);
    return $array;
  }

  /**
   * @param \Error $exception
   * @param string $method
   * @param string $path_inc_query_string
   * @param string $acc_user_id
   * @return static
   */
  public static function convertException(\Throwable $exception) : CCError {
    if ($exception instanceOf self) {// New or received from downstream.
      $cc_error = $exception;
    }
    else {// An error from elsewhere, output as a CCFailure.
      $trace = $exception->getTrace();
      $cc_error = new CCFailure($exception->getMessage());
      $cc_error->backtrace = $trace;
    }

    // If the Exception/Error came from a downstream node, overwrite these
    // properties with the properties from the error source node.
    if (isset($exception->node)) {
      $cc_error->node = $exception->node;
      $cc_error->method = $exception->method;
      $cc_error->path = $exception->path;
      $cc_error->user = $exception->user;
      $cc_error->message = $exception->getMessage();
    }
    return $cc_error;
  }

  /**
   * @return \ReflectionClass
   */
  private static function getReflection() {
    $class = get_called_class();
    return new \ReflectionClass($class);
  }

}
