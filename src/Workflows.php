<?php
namespace CreditCommons;

use CreditCommons\NodeRequester;
use CreditCommons\Workflow;
use CreditCommons\Exceptions\DoesNotExistViolation;

/**
 * Class for clients and base class for nodes. Override this class to incorporate
 * locally defined workflows.
 */
class Workflows {

  private $trunkwardNodeRequester;

  /**
   * The tree shows where workflows originate between here and the trunk, and
   * holds the most localised version of each;
   * @todo this should be stored, not regenerated from the trunkwards nodes on every request.
   * @var array
   */
  public $tree = [];

  function __construct($node_name, NodeRequester $trunkward_node = NULL) {
    // N.B. nodes might not have a trunkward node.
    $this->trunkwardNodeRequester = $trunkward_node;
    $this->tree = $this->loadAll();
  }

  /**
   * This function seems is a mere alias
   */
  function loadAll() {
    static $data = [];
    if (empty($data) and $this->trunkwardNodeRequester) {
      $data = $this->trunkwardNodeRequester->getWorkflows();
    }
    return $data;
  }

  /**
   * @param type $needed_id
   * @param array $all_workflows
   * @return Workflow
   * @throws DoesNotExistViolation
   */
  function get($needed_id) : Workflow {
    foreach ($this->tree as $wfs) {
      foreach ($wfs as $workflow) {
        if ($workflow->id == $needed_id) {
          return $workflow;
        }
      }
    }
    throw new DoesNotExistViolation(type: 'workflow', id: $needed_id);
  }

  /**
   * Get a list of active workflows available for making transactions.
   * @return array
   */
  function getActiveWorkflowIds() : array {
    foreach ($this->tree as $wfs) {
      foreach ($wfs as $wf) {
        if ($wf->active) {
          $ids[] = $wf->id;
        }
      }
    }
  }

}


