<?php

namespace CreditCommons;
use CreditCommons\CreateFromValidatedStdClassTrait;
use CreditCommons\Exceptions\DoesNotExistViolation;
use CreditCommons\Exceptions\DeprecatedWorkflowViolation;
use CreditCommons\Exceptions\WorkflowViolation;

/**
 * Receive and validate new transaction data coming from the leaf/client, and
 * convert it to an internal transaction object.
 */
class NewTransaction {
  use CreateFromValidatedStdClassTrait;

  function __construct(
    public string $uuid,
    public string $payee,
    public string $payer,
    public $quant,
    public string $description,
    public string $type,
    public \stdClass $metadata,
  ){ }

  /**
   * @param \stdClass $data
   */
  static function create(\stdClass $data, Workflows $workflows, $current_user_id) {
    // Set defaults.
    $data->metadata = $data->metadata ?? new \stdClass();
    if (!$workflow = $workflows->get($data->type)) {
      throw new DoesNotExistViolation(type: 'workflow', id: $data->type);
    }
    elseif (!$workflow->active) {
      throw new DeprecatedWorkflowViolation(id: $data->type);
    }
    $direction = $workflow->direction;

    if (empty($data->payee)) {
      $data->payee = $current_user_id;
    }
    elseif (empty($data->payer)) {
      $data->payee = $current_user_id;
    }

    // otherwise it's a 3rd party transaction with payer and payee given.
    $data->uuid = $data->uuid ?? sprintf(
      '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
      mt_rand(0, 0xffff),
      mt_rand(0, 0xffff),
      mt_rand(0, 0xffff),
      mt_rand(0, 0x0fff) | 0x4000,
      mt_rand(0, 0x3fff) | 0x8000,
      mt_rand(0, 0xffff),
      mt_rand(0, 0xffff),
      mt_rand(0, 0xffff)
    );

    static::validateFields($data);
    // Check that the direction corresponds with the creator, which is the last part of the path.
    if ($direction == 'bill') {
      $payee = strrchr($data->payee, '/') ? basename(strrchr($data->payee, '/')) : $data->payee;
      if ($payee <> $current_user_id) {
        throw new WorkflowViolation(type: $data->type, from: 'init', to: $workflow->creation->state);
      }
    }
    elseif ($direction == 'credit') {
      $payer = strrchr($data->payer, '/') ? basename(strrchr($data->payer, '/')) : $data->payer;
      if ($payer <> $current_user_id) {
        throw new WorkflowViolation(type: $data->type, from: 'init', to: $workflow->creation->state);
      }
    }
    return new static(
      uuid: $data->uuid,
      payee: $data->payee,
      payer: $data->payer,
      quant: $data->quant,
      type: $data->type,
      description: $data->description,
      metadata: $data->metadata ?? new stdClass()
    );
  }

}

