<?php

namespace CreditCommons;

/**
 * Store basic info for constructing an error object.
 *
 * Object is created early on, using info from the $request, then is used to
 * construct the error object that is relayed back to the client, indicating
 * where in the tree the error was triggered. The application must create this
 * before calling any other nodes.
 * @code
 *   CreditCommons\ErrorContext::create(user: $user_id, node: $node_name);
 * @endcode
 */
class ErrorContext {

  /**
   * @param string $user
   * @param string $node
   * @param string $path
   * @param string $method
   */
  function __construct(
    public string $user,
    public string $node,
    public string $path = '',
    public string $method = '',
  ) {
  }

  static function create(string $user, string $node, string $path = NULL, string $method = NULL) {
    global $error_context;
    if (!$path) {
      $path = $_SERVER['REQUEST_URI']??'';
    }
    if (!$method) {
      $method = $_SERVER['REQUEST_METHOD']??'';
    }
    $error_context = new static($user, $node, $path, $method);
  }

}
