<?php

namespace CreditCommons\Leaf;

use CreditCommons\BaseTransaction;
use CreditCommons\EntryFlat;
use CreditCommons\Leaf\TransactionInterface;

/**
 * Transaction for client side display including transitions property.
 */
abstract class LeafTransaction extends BaseTransaction implements TransactionInterface {

  public array $transitions = [];

  /**
   * @param stdClass[] $rows
   *   Which are Entry objects flattened by json for transport.
   * @return Entry[]
   *   The created entries
   */
  protected static function upcastEntries(array &$rows, bool $is_additional = FALSE) : bool {
    $entries = [];
    foreach ($rows as $row) {
      // same as an entry but with strings as account names.
      $entries[] = EntryFlat::create($row);
    }
    return $entries;
  }


}
