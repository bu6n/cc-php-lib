<?php

namespace CreditCommons;

use CreditCommons\Requester;
use CreditCommons\TradeStats;
use CreditCommons\BaseTransaction;
use CreditCommons\Exceptions\CCFailure;
use CreditCommons\Exceptions\CCError;
use CreditCommons\Exceptions\UnavailableNodeFailure;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Exception\ClientException;

/**
 * Class for calling credit commons nodes, for use by nodes and clients.
 * - Adds extra headers to each downstream request
 * - Catches errors from Guzzle and throws them again as CC Errors.
 */
class NodeRequester extends Requester implements CreditCommonsInterface {

  public $nodeName;
  /**
   * @todo review what this is for.
   * @var bool
   */
  protected bool $catch = FALSE;

  /**
   * Build a request from an upstream node to a downstream node.
   */
  function __construct(AccountRemoteInterface $remote_account, string $current_node_name) {
    global $cc_user;
    parent::__construct($remote_account->getUrl());
    $this->nodeName = $current_node_name;
    // Authentication
    $this->setHeader('CC-User', $this->nodeName);
    $this->setHeader('CC-Auth', $remote_account->getLastHash());
    if (!($cc_user instanceOf AccountRemoteInterface)){
      $this->catch = TRUE;
    }
  }

  /**
   * {@inheritDoc}
   * @todo ensure the result->data is in the right format.
   */
  public function getOptions() : array {
    return (array)$this->setMethod('options')->request()->data;
  }

  /**
   * {@inheritDoc}
   * @todo ensure the result->data is in the right format.
   */
  public function getAbsolutePath() : array {
    return $this->request('absolutepath')->data;
  }

  /**
   * {@inheritDoc}
   * @todo ensure the result->data is in the right format.
   */
  public function handshake() : array {
    $temp = $this->catch;
    $this->catch = TRUE;
    if ($hs = $this->request('handshake')) {
      $this->catch = $temp;
      return (array)$hs->data;
    }
    throw new UnavailableNodeFailure($this->baseUrl);
  }

  /**
   * {@inheritDoc}
   * @todo ensure the result is in the right format.
   */
  public function filterTransactions(array $params = []) : array {
    // Send only valid params
    $valid = ['payer', 'payee', 'involving', 'before', 'after', 'uuid', 'quant', 'description', 'states', 'type', 'is_primary', 'uuid'];
    $params = array_intersect_key($params, array_flip($valid));
    if (isset($params['entries']) and $params['entries'] <> 'false') {
      $params['entries'] = 'true';// this is how to send a boolean in a querystring.
    }
    else {
      unset($params['entries']);
    }
    $this->options[RequestOptions::QUERY] = $params;
    // NB there is also the meta property which SHOULD have the pager in it
    $result = $this->request('transactions');
    return [$result->data, $result->meta, $result->links];
  }

  /**
   * {@inheritDoc}
   * @todo ensure the result is in the right format.
   */
  public function filterTransactionEntries(array $params = []) : array {
    // Send only valid params
    $valid = ['payer', 'payee', 'involving', 'before', 'after', 'uuid', 'quant', 'description', 'states', 'type', 'is_primary', 'uuid'];
    $params = array_intersect_key($params, array_flip($valid));
    $this->options[RequestOptions::QUERY] = $params;
    // NB there is also the meta property which SHOULD have the pager in it
    $result = $this->request('entries');
    return [$result->data, $result->links];
  }

  /**
   * {@inheritDoc}
   * @todo ensure the result is in the right format.
   */
  public function getTransaction(string $uuid) : array {
    $result = $this->request("transaction/$uuid");
    return [$result->data, $result->meta->transitions];
  }

  /**
   * {@inheritDoc}
   * @todo ensure the result->data is in the right format.
   */
  public function getTransactionEntries(string $uuid) : array {
    return $this->request("entries/$uuid")->data;
  }

  /**
   * {@inheritDoc}
   * @todo ensure the result->data is in the right format.
   */
  public function accountNameFilter(string $acc_path = '', $limit = 10) : array {
    $path = "account/names";
    if ($acc_path) {
      $params[] = "acc_path=$acc_path";
    }
    $params[] = "limit=$limit";
    try {
      return $this->request("$path?".implode('&', $params))->data;
    }
    catch (CCError $e){}
    return [];
  }

  /**
   * {@inheritDoc}
   * @todo ensure the result->data is in the right format.
   */
  function getAccountLimits(string $acc_path = '') : array {
    $path = 'account/limits';
    if ($acc_path) {
      //static::prepareAccPath($acc_path);
      $path .= '?acc_path='. $acc_path;
    }
    // Keyed array is interpreted as object by json_decode.
    return (array)$this->request($path)->data;
  }

  /**
   * {@inheritDoc}
   * @todo ensure the result->data is in the right format.
   */
  public function getAccountSummary(string $acc_path = '') : array {
    $path = 'account/summary';
    if ($acc_path) {
      //static::prepareAccPath($acc_path);
      $path .= '?acc_path='. $acc_path;
    }
    $all_results = $this->request($path)->data;
    $all_stats = [];
    foreach ($all_results as $acc_id => $results) {
      $stats = new \stdClass;
      $stats->pending = TradeStats::create($results->pending);
      $stats->completed = TradeStats::create($results->completed);
      $all_stats[$acc_id] = $stats;
    }
    return $all_stats;
  }

  /**
   * {@inheritDoc}
   * @todo ensure the result->data is in the right format.
   */
  public function getAccountHistory(string $acc_path, int $samples = 0) : array {
    if ($samples) {
      $this->options[RequestOptions::QUERY] = ['samples' => $samples];
    }
    $path = "account/history";
    if ($acc_path) {
      //static::prepareAccPath($acc_path);
      $path .= '?acc_path='. $acc_path;
    }
    $result = $this->request($path)->data;
    return (array)$result;
  }

  /**
   * {@inheritDoc}
   */
  public function transactionChangeState(string $uuid, string $target_state) : void {
    $expected = $target_state == 'null' ? 200 : 201;
    $this->setMethod('patch')
      ->request("transaction/$uuid/$target_state");
  }

  /**
   * {@inheritDoc}
   *
   * the resulting structure of the tree needs to show what workflows are
   * available at each level, which each workflow (with the same hash) having
   * been translated in the nearer branches.
   * The structure received should be
   * ['trunk' => ['wf1', 'wf2'], 'branch' => ['wf3']]
   * This function loads the local translations
   *
   * @note This is only ever called if the node is trunkward.
   */
  public function getWorkflows() : array {
    $data = (array)$this->request('workflows')->data;
    if (empty($data)) {
      throw new CCFailure('No active trunkwards workflows were found.');
    }
    $tree = [];
    foreach ($data as $node_name => $defs) {
      foreach ($defs as $def) {
        $workflow = new Workflow($def);
        $tree[$node_name][$workflow->getHash()] = $workflow;
      }
    }
    return $tree;
  }

  /**
   * {@inheritDoc}
   * @todo ensure the result->data is in the right format.
   */
  public function buildValidateRelayTransaction(BaseTransaction $transaction) : array {
    $result = $this
      ->setMethod('post')
      ->setBody($transaction) // Via jsonSerialize
      ->request('transaction/relay');
    return (array)$result->data; // The additional rows
  }


  /**
   * {@inheritDoc}
   * @todo ensure the result->data is in the right format.
   */
  public function about(string $node_path) : \stdClass {
    return $this->request("about?node_path=$node_path")->data;
  }

  /**
   * {@inheritDoc}
   */
  protected function request(string $endpoint = '/') :\stdClass|NULL {
    // Guzzle will strip the path off $this->baseUrl, so if is a path prefix
    // copy it from the baseUrl to the endpoint.
    $baseUrl = $this->baseUrl;
    $parts = parse_url($this->baseUrl);
    if (isset($parts['path'])) {
      $endpoint = $parts['path'] .'/'.$endpoint;
      $baseUrl = $parts['scheme'].'://'.$parts['host'];
      if (isset($parts['port'])){
        $baseUrl .= ':'.$parts['port'];
      }
    }
    $params = ['base_uri' => $baseUrl, 'timeout' => 2];

    try{
      $client = new Client($params);
      $response = $client->{$this->method}($endpoint, $this->options);
    }
    catch (ConnectException $e) {
      // The request timed out.
      throw new UnavailableNodeFailure(unavailable_url: $this->baseUrl);
    }
    // ClientException is Guzzle's way of returning 40x and 50x responses
    catch (ClientException|ServerException $e) {// This includes CCError from downstream
      $body = $e->getResponse()->getBody();
      $contents = $body->getContents();
      if(strlen($contents)) {
        if ($result = json_decode($contents)) {
          if (isset($result->errors) and !is_array($result->errors)) {
            $result->errors = (array)$result->errors;
          }
          if (isset($result->errors[0])) {
            throw self::reconstructCCErr($result->errors[0]);
          }
        }
        else {
          throw new CCFailure("$this->method $baseUrl/$endpoint: $contents");
        }
      }
      else {
        $message = "Empty ". $e->getCode() ." response from $this->method $this->baseUrl/$endpoint";
        if (isset($this->options[RequestOptions::BODY])) {
          $message .= " with request body\n".$this->options[RequestOptions::BODY];
        }
        throw new CCFailure($message);
      }
    }
    catch (\Exception $e) { // Should never happen.
      // This is not likely to be caught
      $mess = "Unexpected error from $this->method $this->baseUrl/$endpoint: ";
      throw new CCFailure($e->getCode() ." $mess: ". $e->getMessage());
    }
    // The result is not an error.
    return $this->processResponse($response);
  }

  /**
   * Process the Response object
   * @todo badly formatted responses could cause unpredictable behaviour
   * including throwing php warnings. All valid responses should be found in the
   * responses 'data' property. So check for response->data and return that.
   */
  protected function processResponse($response) : \stdClass|NULL {
    $raw_result = strval($response->getBody());
    if ($raw_result and $response->getHeaderline('Content-Type') != 'application/json') {
      // @todo are ALL responses Json or are some null or some text?
      throw new CCFailure($response->getHeaderline('Content-Type') ." non-json response: '$raw_result'");
    }
    $result = json_decode($raw_result);
    if ($raw_result and $raw_result != 'null' and is_null($result)) {
      throw new CCFailure("Json expected but not delivered: '$raw_result'");
    }
    return $result;
  }

  /**
   * Upcast an exception passed from downstream as a json object back to CCError
   * @param \stdClass $remote_error
   *   Exception reconstructed from json response.
   * @return \CCError
   * @throws \Exception
   */
  static function reconstructCCErr(\stdClass $remote_error) : CCError {
    $error_class = "\CreditCommons\Exceptions\\$remote_error->class";
    if (empty($remote_error->class) or !class_exists($error_class)) {
      throw new \Exception('Unexpected exception type: '.print_r($error_class, 1));
    }
    $base_properties = ['node', 'class', 'method', 'path', 'line', 'user', 'backtrace'];
    $other_arg_names = array_diff_key(
      get_class_vars($error_class),
      array_flip($base_properties)
    );
    // Prepare an array of named args.
    $args = [];
    foreach (array_keys($other_arg_names) as $arg_name) {
      $args[$arg_name] = $remote_error->{$arg_name};
    }
    $error_class::validateCastParams($args);

    $ccError = new $error_class(...$args);
    $ccError->node = $remote_error->node;
    $ccError->path = $remote_error->path;
    $ccError->method = $remote_error->method;
    $ccError->user = $remote_error->user;
    $ccError->backtrace = $remote_error->backtrace;
    return $ccError;
  }

}
